# Zabbix - An Open Source Monitoring Software

[Zabbix](https://www.zabbix.com/features) is an open-source monitoring software used for real-time monitoring of softwares, network, hardware, etc.

Even though we have many options to pick when we consider a monitoring tool, Zabbix usually stands out not only because it's an open-source software but because it's easy to start with and is able to be heavily customised, having performance upgrades with each release and more importantly, it's solid and reliable.

Still, although being very user-friendly, the task of setting it up can be somewhat complex for first timers and it also requires considerable knowledge of databases, operating systems and web softwares. This escalates further when our requirements include security, performance tuning, and so on.

Although this guide is far from being considered advanced as we'll overlook many security configurations that should be done in a production environment, we'll cover a more intermediate config, with a distributed setup - a Zabbix server, a database server running [PostgreSQL](https://www.postgresql.org/about/) and a frontend server running [NGINX](https://www.nginx.com/resources/glossary/nginx/). I will be leaving [SELinux](https://www.redhat.com/en/topics/linux/what-is-selinux) in enforcing mode so we'll certainly have to deal with it and I'll be setting up [VDO, Virtual Data Optimizer](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deduplicating_and_compressing_logical_volumes_on_rhel/con_introduction-to-vdo-on-lvm_deduplicating-and-compressing-logical-volumes) for the database cluster.

## Requirements
Firstly, I'll be assuming you have intermediate knowledge of Linux systems and know your way around the CLI. You should be able to check log files and use tools to troubleshoot some issues regarding network and services. Although I'll be helping fixing some SELinux issues, it's possible to encounter a different situation than mine.

Even though I'm running [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux) with [Developer Subscription](https://developers.redhat.com/products), you're more than able to follow up with [CentOS](https://www.centos.org/) if you decide so.

I'll be using 3 virtual machines in [KVM](https://www.linux-kvm.org/page/Main_Page) with the following specs:

- Zabbix Server     KVSL-ZBX-180 / 1vCPU, 1024MiB RAM and 20GB Disk / 10.20.20.180
- Frontend Server   KVSL-NGX-181 / 1vCPU, 1024MiB RAM and 20GB Disk / 10.20.20.181
- Database Server   KVSL-PGS-182 / 2vCPU, 2048MiB RAM and 2x 20GB Disk / 10.20.20.182

## Database Server - PostgreSQL
We'll start with the database server where we'll be preparing a VDO volume for the database cluster, editing some [unit files](https://www.freedesktop.org/software/systemd/man/systemd.unit.html) for the service and the [mount unit](https://www.freedesktop.org/software/systemd/man/systemd.mount.html); we'll then set up the database and configure it for both the Zabbix and Frontend servers to connect to it.

1. Install the VDO package and set up the volume and Filesystem:
```
# dnf install vdo -y
# vdo create --name=vdo_db01 --device=/dev/sdb --vdoLogicalSize=200G
# mkfs.xfs -K /dev/mapper/vdo_db01
```
2. Now we will set up a mount unit in systemd so we can control the mount point. For that I'll copy an example from the VDO doc folder and start from there:
```
# cp /usr/share/doc/vdo/examples/systemd/VDO.mount.example /etc/systemd/system/opt-pgsql.mount
```
3. Edit the unit file. 
*Keep in mind the mount point and the unit filename. The filename has to follow this standard: /path/to/mountpoint -> path-to-mountpoint.mount otherwise it may fail to mount.*
```
# vim /etc/systemd/system/opt-pgsql.mount
======================================
[Unit]
Description = VDO Volume for Postgresql Data
name = opt-pgsql.mount
Requires = vdo.service systemd-remount-fs.service
After = network.target
Conflicts = umount.target
 
[Mount]
What = /dev/mapper/vdo_db01
Where = /opt/pgsql
Type = xfs
Options = discard

[Install]
WantedBy = multi-user.target
======================================
```
4. Now our mount unit is ready to be started and we can prepare the directory for Postgres:
*Note that we can't forget we're running SELinux on enforcing mode. When we create the directories for our database, we have to make sure the proper context is set*
***In order to set the context I'll use the semanage tool. If you're running a minimal install like me and don't have it at first, be sure to install the policycoreutils-python-utils package***
```
# mkdir /opt/pgsql && systemctl enable --now opt-pgsql.mount && mkdir /opt/pgsql/data
# semanage fcontext -a -t postgresql_db_t "/opt/pgsql(/.*)?" && restorecon -Rvv /opt/pgsql
```
5. Install postgres. I'll go for ver. 12
```
# dnf module install postgresql:12
```
6. As we're changing the data dir, we'll have to edit the unit file for the service to change its ENV variables and set it to start after VDO is loaded.
```
# systemctl edit postgresql.service
======================================
[Unit]
RequiresMountsFor=/opt/pgsql

[Service]
Environment=PGDATA=/opt/pgsql/data
Environment=PGLOG=/opt/pgsql/data/log
======================================
```
7. Change permissions for the postgres dir and initialize the database cluster:
```
# chown -R postgres: /opt/pgsql && chmod -R 700 /opt/pgsql
# su - postgres -c "initdb -D /opt/pgsql/data"
```
8. Create the Zabbix user and database:
```
# sudo -u postgres createuser --pwprompt zabbix
# sudo -u postgres createdb -O zabbix zabbix
```
9. Configure /opt/pgsql/data/postgresql.conf:
*I'll be changing only two variables here - [listen_addresses] so our database listens to external requests, and [password_encryption] so we use [SCRAM](https://tools.ietf.org/html/rfc7677) for authentication:
```
# vim /opt/pgsql/data/postgresql.conf
======================================
password_encryption = scram-sha-256
listen_addresses = '10.20.20.182'
======================================
```
10. Configure the Postgres [Client Authentication](https://www.postgresql.org/docs/12/auth-pg-hba-conf.html):
*This file is important so we can set the way we're going to define the authentication method to our database. We've setup both the user and database zabbix, and we also have the addresses these requests are coming from so we can be more restrictive using CIDR ranges:*
```
# vim /opt/pgsql/data/pg_hba.conf
======================================
host    zabbix  zabbix  10.20.20.180/31 scram-sha-256
======================================
```
11. Allow the postgresql service on the firewall:
```
# firewall-cmd --add-service=postgresql --permanent && firewall-cmd --reload
```
12. Enable and start the postgres service:
```
# systemctl enable --now postgresql.service
```
And that's it. Now we've got our postgres server running and it's time for the next in line.
*Note that although we've finished setting up the database configuration, we'll still have to import the initial schema from the Zabbix server once we set it up.*

## Frontend Server - NGINX
As I don't have any specific requirement, I'll be getting Zabbix from [their repository](https://repo.zabbix.com/), so we will install it here and on the Zabbix server. But if you want to get Zabbix [from source](https://www.zabbix.com/download_sources), feel free to do so.

1. Install the Zabbix repository:
```
# rpm -Uvh https://repo.zabbix.com/zabbix/5.2/rhel/8/x86_64/zabbix-release-5.2-1.el8.noarch.rpm
```
2. Install Nginx from the Zabbix repository and the plugin php-pgsql so our frontend can connect to the Postgresql server later on during the installation:
```
# dnf install zabbix-nginx-conf php-pgsql
```
3. Allow the https service in the firewalld as we're setting up the Frontend with HTTPs:
```
# firewall-cmd --add-service=https --permanent && firewall-cmd --reload
```
4. Uncomment the [date.timezone] in the Zabbix configuration file for [PHP-FPM](https://www.php.net/manual/en/install.fpm.php), and set it to your timezone:
*You may also change the user/group configuration, but you should know that you'll have to make the changes for some files where appropriate*
```
# vim /etc/php-fpm.d/zabbix.conf
======================================
php_value[date.timezone] = America/Sao_Paulo
======================================
```
5. Generate a self-signed certificate with [openssl](https://www.openssl.org/):
```
# openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/private/nginx-selfsigned.key -out /etc/nginx/ssl/certs/nginx-selfsigned.crt

Generating a RSA private key
...........+++++
..........+++++
writing new private key to '/etc/nginx/ssl/private/nginx-selfsigned.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:.
State or Province Name (full name) []:.
Locality Name (eg, city) [Default City]:.
Organization Name (eg, company) [Default Company Ltd]:.
Organizational Unit Name (eg, section) []:.
Common Name (eg, your name or your server's hostname) []:example.net
```
6. Edit the Zabbix configuration file for Nginx:
*We'll be setting the Frontend to HTTPS and configuring Nginx to enable Zabbix on the root directory of the URL.*
```
# vim /etc/nginx/conf.d/zabbix.conf
======================================
server {
        listen          443 ssl;
        server_name     zabbix.example.net;
        index           index.php;

        ssl_certificate         /etc/nginx/ssl/certs/nginx-selfsigned.crt;
        ssl_certificate_key     /etc/nginx/ssl/private/nginx-selfsigned.key;
        ssl_protocols           TLSv1.2;
        ssl_ciphers             HIGH:!aNULL:!MD5;

        root    /usr/share/zabbix;
        
        location / {
                 try_files      $uri $uri/      /index.php?$args;
        }
======================================
```
7. Enable the Nginx and PHP-FPM services:
```
# systemctl enable --now nginx php-fpm
```
8. Set the [SELinux booleans](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/sect-security-enhanced_linux-working_with_selinux-booleans) required for our httpd service to connect to both our database and zabbix server:
```
# setsebool -P httpd_can_network_connect_db 1 && setsebool -P httpd_can_connect_zabbix 1
```
And we're done with our Frontend Server.

## Zabbix Server
The last part in this tutorial - the Zabbix server. Although it's supposed to be the fastest server to set up with the default confs, it can become somewhat troublesome if we leave SELinux in enforcing mode and we don't know how to deal with it. So let's take a look at it.

1. First let's install the Zabbix repository again:
```
# rpm -Uvh https://repo.zabbix.com/zabbix/5.2/rhel/8/x86_64/zabbix-release-5.2-1.el8.noarch.rpm
```
2. Now install the necessary packages:
```
# dnf install zabbix-server-pgsql zabbix-agent
```
3. Edit the Zabbix server configuration file:
```
# vim /etc/zabbix/zabbix_server.conf
======================================
DBName=zabbix
DBUser=zabbix
DBPassword=<password>
DBPort=5432
======================================
```
4. Import the initial schema into our remote database server:
*It's possible that if you missed something, you might get an error here. Among the most probable are network issues (was port 5432 opened on the database server?) and configuration issues (is the /opt/pgsql/data/pg_hba.conf properly configured? Does the zabbix user have the correct credentials?)*
```
# zcat /usr/share/doc/zabbix-server-pgsql/create.sql.gz | sudo -u zabbix psql -h 10.20.20.182 -U zabbix zabbix
```
5. Now all we have to do, in theory, is start the Zabbix service and we're done:
```
# systemctl enable --now zabbix-server
```
If the service has gone up, then awesome. You're good to go! Just visit <hostname.domain.TLD> and you should see the Zabbix Installation page. But if it didn't and upon issuing a systemctl status you've got the output below, then buckle up as we have more to do:
```
-- Unit zabbix-server.service has begun starting up.
Mar 14 19:53:23 KVSL-ZBX-180.example.net zabbix_server[1556]: zabbix_server [1556]: Cannot initialize IPC services: Cannot access path "/var/run/zabbix": [13] Permission denied.
Mar 14 19:53:23 KVSL-ZBX-180.example.net systemd[1]: zabbix-server.service: Control process exited, code=exited status=1
Mar 14 19:53:23 KVSL-ZBX-180.example.net systemd[1]: zabbix-server.service: Failed with result 'exit-code'.
```
## Troubleshooting SELinux and Zabbix
To begin troubleshooting SELinux issues, we'll need some tools provided by two packages:
- setroubleshoot
- policycoreutils-python-utils

After installing them, we can start troubleshooting with [ausearch](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-searching_the_audit_log_files). From that last output we've got, we have a timestamp to pass to ausearch and lessen our troubles:
```
# ausearch -m AVC -ts 19:53:20 -te 19:53:30 

----
time->Sun Mar 14 19:53:23 2021
type=PROCTITLE msg=audit(1615762403.681:85): proctitle=2F7573722F7362696E2F7A61626269785F736572766572002D63002F6574632F7A61626269782F7A61626269785F7365727665722E636F6E66
type=SYSCALL msg=audit(1615762403.681:85): arch=c000003e syscall=21 success=no exit=-13 a0=55be719ace40 a1=6 a2=7fffcfb7c890 a3=0 items=0 ppid=1 pid=1556 auid=4294967295 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=(none) ses=4294967295 comm="zabbix_server" exe="/usr/sbin/zabbix_server_pgsql" subj=system_u:system_r:zabbix_t:s0 key=(null)
type=AVC msg=audit(1615762403.681:85): avc:  denied  { dac_override } for  pid=1556 comm="zabbix_server" capability=1  scontext=system_u:system_r:zabbix_t:s0 tcontext=system_u:system_r:zabbix_t:s0 tclass=capability permissive=0
```
The message we've got wasn't much clearer but we can already start grasping what happened. The output already shows an AVC message stating that SELinux denied a dac_override issued by the zabbix-server service.
*Note: When SELinux denies an action, like this action where Zabbix maybe tried to start and create a pid file, an* [Access Vector Cache, AVC](https://wiki.gentoo.org/wiki/SELinux/Logging) *message is logged to /var/log/audit/audit.log and /var/log/messages. Knowing that, we can try narrowing our search going for either AVC or USER_AVC messages, which are the most common audit messages.*

Before jumping into conclusions, let's delve further and use [setroubleshoot](https://linux.die.net/man/8/setroubleshootd) with [journalctl](https://www.freedesktop.org/software/systemd/man/journalctl.html) to see if we can get more detail:
```
journalctl -t setroubleshoot --since=19:53:20 --until=19:53:30

-- Logs begin at Sun 2021-03-14 19:49:57 -03, end at Sun 2021-03-14 20:18:13 -03. --
Mar 14 19:53:23 KVSL-ZBX-180.example.net setroubleshoot[1524]: AnalyzeThread.run(): Cancel pending alarm
Mar 14 19:53:26 KVSL-ZBX-180.example.net setroubleshoot[1524]: SELinux is preventing /usr/sbin/zabbix_server_pgsql from using the dac_override capability. For complete SELinux messages run: sealert -l 160ced14-0e9c-4bcd-85ae-9b170b844932
Mar 14 19:53:26 KVSL-ZBX-180.example.net setroubleshoot[1524]: SELinux is preventing /usr/sbin/zabbix_server_pgsql from using the dac_override capability.
                                                               
                                                               *****  Plugin dac_override (91.4 confidence) suggests   **********************
                                                               
                                                               If you want to help identify if domain needs this access or you have a file with the wrong permissions on your system
                                                               Then turn on full auditing to get path information about the offending file and generate the error again.
                                                               Do
                                                               
                                                               Turn on full auditing
                                                               # auditctl -w /etc/shadow -p w
                                                               Try to recreate AVC. Then execute
                                                               # ausearch -m avc -ts recent
                                                               If you see PATH record check ownership/permissions on file, and fix it,
                                                               otherwise report as a bugzilla.
                                                               
                                                               *****  Plugin catchall (9.59 confidence) suggests   **************************
                                                               
                                                               If you believe that zabbix_server_pgsql should have the dac_override capability by default.
                                                               Then you should report this as a bug.
                                                               You can generate a local policy module to allow this access.
                                                               Do
                                                               allow this access for now by executing:
                                                               # ausearch -c 'zabbix_server' --raw | audit2allow -M my-zabbixserver
                                                               # semodule -X 300 -i my-zabbixserver.pp
```
Now that's much better. But we can go even further with another tool, [sealert](https://linux.die.net/man/8/sealert). This time though, not only I want that info displayed for me, but I'll direct the output to a file so we can save it for later. So let's follow setroubleshoot's suggestion and run sealert:
```
# sealert -l 160ced14-0e9c-4bcd-85ae-9b170b844932 | tee zabbix_avc_dac-override.log

=====================================================================================================================
SELinux is preventing /usr/sbin/zabbix_server_pgsql from using the dac_override capability.

*****  Plugin dac_override (91.4 confidence) suggests   **********************

If you want to help identify if domain needs this access or you have a file with the wrong permissions on your system
Then turn on full auditing to get path information about the offending file and generate the error again.
Do

Turn on full auditing
# auditctl -w /etc/shadow -p w
Try to recreate AVC. Then execute
# ausearch -m avc -ts recent
If you see PATH record check ownership/permissions on file, and fix it,
otherwise report as a bugzilla.

*****  Plugin catchall (9.59 confidence) suggests   **************************

If you believe that zabbix_server_pgsql should have the dac_override capability by default.
Then you should report this as a bug.
You can generate a local policy module to allow this access.
Do
allow this access for now by executing:
# ausearch -c 'zabbix_server' --raw | audit2allow -M my-zabbixserver
# semodule -X 300 -i my-zabbixserver.pp


Additional Information:
Source Context                system_u:system_r:zabbix_t:s0
Target Context                system_u:system_r:zabbix_t:s0
Target Objects                Unknown [ capability ]
Source                        zabbix_server
Source Path                   /usr/sbin/zabbix_server_pgsql
Port                          <Unknown>
Host                          KVSL-ZBX-180.example.net
Source RPM Packages           zabbix-server-pgsql-5.2.5-1.el8.x86_64
Target RPM Packages           
SELinux Policy RPM            selinux-policy-targeted-3.14.3-54.el8_3.2.noarch
Local Policy RPM              selinux-policy-targeted-3.14.3-54.el8_3.2.noarch
Selinux Enabled               True
Policy Type                   targeted
Enforcing Mode                Enforcing
Host Name                     KVSL-ZBX-180.example.net
Platform                      Linux KVSL-ZBX-180.example.net
                              4.18.0-240.15.1.el8_3.x86_64 #1 SMP Wed Feb 3
                              03:12:15 EST 2021 x86_64 x86_64
Alert Count                   149
First Seen                    2021-03-14 19:53:13 -03
Last Seen                     2021-03-14 20:18:10 -03
Local ID                      160ced14-0e9c-4bcd-85ae-9b170b844932

Raw Audit Messages
type=AVC msg=audit(1615763890.976:678): avc:  denied  { dac_override } for  pid=2789 comm="zabbix_server" capability=1  scontext=system_u:system_r:zabbix_t:s0 tcontext=system_u:system_r:zabbix_t:s0 tclass=capability permissive=0


type=SYSCALL msg=audit(1615763890.976:678): arch=x86_64 syscall=access success=no exit=EACCES a0=5577142b3e40 a1=6 a2=7ffffc903850 a3=0 items=0 ppid=1 pid=2789 auid=4294967295 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=(none) ses=4294967295 comm=zabbix_server exe=/usr/sbin/zabbix_server_pgsql subj=system_u:system_r:zabbix_t:s0 key=(null)

Hash: zabbix_server,zabbix_t,zabbix_t,capability,dac_override
=====================================================================================================================
```
And here we are. Now we know for sure that SELinux denied zabbix-server the dac_override capability, a capability that grants a process the ability to bypass file read, write and execute permission checks (see [man capabilities](https://man7.org/linux/man-pages/man7/capabilities.7.html)). For this specific capability, we know that most likely the zabbix-service tried to access an specific file/directory, /var/run/zabbix to be precise, with a wrong set of permissions/ownership and though it should've worked in another situation, SELinux originally denies that capability.

Leaving details aside, we should know at least that this kind of situation usually happens when a service, though using a non-privileged user to run, is first run as root before becoming non-root, and during that process trying to access its directory with a different set of uid/guid. Although root is supposed to have full access to the system, SELinux prevents the service from using its privileges that way and blocks it.

So now that we know what our issue is, we can think of two options for fixing this:

1. Generate and install a SELinux module granting CAP_DAC_OVERRIDE to zabbix_t context or,
2. Grant root access via [DAC](https://www.cs.cornell.edu/courses/cs5430/2015sp/notes/dac.php), which in other words means changing uid/guid permissions.

Although we can grant cap_dac_override to the zabbix service to fix this issue, it also means that the service will have this capability system-wide which may raise a safety concern. Surely, if we see that we're dealing with lots of issues recurring from this, maybe this would be a good option, but if we allow access to root via DAC, what I assume is the approach of least privilege, this shouldn't bother SELinux anymore and work. So, in other words, let's adjust the permissions in the /var/run/zabbix dir and try running the service again:
```
# chown :root /var/run/zabbix && chmod 760 /var/run/zabbix
# systemctl start zabbix-server.service && systemctl status -l zabbix-server.service

● zabbix-server.service - Zabbix Server
   Loaded: loaded (/usr/lib/systemd/system/zabbix-server.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2021-03-14 20:56:23 -03; 6s ago
  Process: 8086 ExecStop=/bin/kill -SIGTERM $MAINPID (code=exited, status=0/SUCCESS)
  Process: 8109 ExecStart=/usr/sbin/zabbix_server -c $CONFFILE (code=exited, status=0/SUCCESS)
 Main PID: 8111 (zabbix_server)
```
And it worked! The zabbix-service is now running properly!

But still, if you want to apply the SELinux policy, you could then run the tool [audit2allow](https://linux.die.net/man/1/audit2allow) pointing to that file we've created with sealert. And for learning purposes, we might as well do that:
```
# cat zabbix_avc_dac-override.log | audit2allow -M zabbix_dac-override
******************** IMPORTANT ***********************
To make this policy package active, execute:

semodule -i zabbix_dac-override.pp
```
We've run audit2allow with the -M option, which creates two files for us with that name we specified, one with extension 'te' and another 'pp'. You could inspect the 'te' file for details on what will be allowed in SELinux should you install that module:
```
# cat zabbix_dac-override.te 

module zabbix_dac-override 1.0;

require {
        type zabbix_t;
        class capability dac_override;
}

#============= zabbix_t ==============
allow zabbix_t self:capability dac_override;
```
And now to install it:
```
# semodule -i zabbix_dac-override.pp
```
And done! You can check if your module was successfully loaded:
```
# semodule -l | grep zabbix_dac

zabbix_dac-override
```
## Final considerations
And that's all! Now we have a distributed Zabbix environment which we can access by going to the address we set and continuing the Web Installation with the instructions found in the [official website](https://www.zabbix.com/documentation/current/manual/installation/frontend).

Although Zabbix is now running, it's very likely that setroubleshoot might still be nagging you with lots of other issues from Zabbix, but if you follow the same steps we did previously, you can solve all of them very easily without changing SELinux to permissive or disabling it altogether, which should never be an option.

And that ends it.

The configuration files can soon be found at my [codeberg repository](https://codeberg.org/cgoslaw/distr-zabbix) here.

If you have any comments, corrections or suggestions you consider appropriate, please feel free to contact me.


Thank you very much,

Cesar Goslawski (cgoslaw)
